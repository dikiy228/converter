import { FormEvent, SyntheticEvent, useEffect, useState } from 'react';
import { useDispatch } from 'react-redux';
import styled from 'styled-components';
import { useTypedSelector } from '../../hooks/useTypedSelector/useTypedSelector';
import { defaultCurrency } from '../../redux/actions/defaultCurrencyAction';
import { RUB, USD } from '../../types/utils';
import { rounding } from '../../useFul';

const Background = styled.div``;

const Title = styled.h2``;

const Select = styled.select`
    max-width: fit-content;
    margin-bottom: 20px;
`;

const Option = styled.option``;

const Wrapper = styled.div`
    display: flex;
    flex-direction: column;
    overflow: hidden;
`;

const Form = styled.form`
    display: flex;
    flex-direction: column;
`;

const Input = styled.input`
    width: 100%;
    height: 30px;
    max-width: 300px;
`;

const Label = styled.label`
    opacity: 0.8;
`;

const ConvertedValue = styled.span<{ converted: boolean }>`
    display: flex;
    align-items: center;
    justify-content: center;
    max-width: 150px;
    height: fit-content;
    min-height: 40px;
    padding: 5px 5px;
    border: 1px solid #000000;
    border-radius: 3px;
    transform: ${({ converted }) => (converted ? "translateY(-100%)" : "translateY(-400%)")};
    margin-left: 30%;
    transition: transform 1s linear;
    color: #ffffff;
`;

const Notification = styled.div`
    width: fit-content;
    height: fit-content;
    padding: 10px 10px;
    border: 1px solid #000000;
    border-radius: 3px;
    color: #ffffff;
    transition: transform 1s linear;
`;

const Strong = styled.strong`
    font-size: 110%;
    text-decoration: underline #000000;
`;

function Converter() {
    const [value, setValue] = useState<string>('');
    const [convertArray, setConvertArray] = useState<string[]>();
    const [converted, setConverted] = useState<number | string>();
    const [open, setOpen] = useState<boolean>(false);
    const [example, setExample] = useState<string>();
    const [noMatches, setNoMatches] = useState<boolean>(false);
    const [prompt, setPrompt] = useState<boolean>(false);
    const { currencysData } = useTypedSelector(state => state);
    const defCurrency = currencysData.defaultCurrency;
    const dispatch = useDispatch();

    useEffect(() => {
        if (value.length) {
            const result = value.split(" ");
            setConvertArray(result);
            if (result.length === 4) {
                setPrompt(true);
            }
        }
    }, [value])

    useEffect(() => {
        if (defCurrency?.includes(RUB)) {
            setExample("Example: 10 rub in usd");
        }
        if (defCurrency?.includes(USD)) {
            setExample("Example: 10 usd in rub");
        }
        setConverted('');
    }, [defCurrency])

    const handleChange = (event: SyntheticEvent<HTMLSelectElement, Event>) => {
        dispatch(defaultCurrency(event.currentTarget.value));
        setOpen(false);
        setNoMatches(false);
    }

    const conversionCurrency = (e: FormEvent<HTMLFormElement>, array?: string[]) => {
        e.preventDefault();
        if (array && array.length === 4) {
            const findCharCode = (str: string) => currencysData.currencys?.find(item => item.CharCode === str);
            let convertedResult;
            setNoMatches(false);
            if (defCurrency?.includes(USD) && array[1] === USD.toLowerCase()) {
                const convertedValue = findCharCode(array[1].toUpperCase());
                if (convertedValue) {
                    convertedResult = rounding(convertedValue?.Value, 100, 100);
                }
            } else if (defCurrency?.includes(RUB) && array[1] === RUB.toLowerCase()) {
                const usdRate = findCharCode(USD);
                if (usdRate) {
                    const rubNum = rounding((1 / usdRate?.Value), 100, 100) * Number(array[0]);
                    convertedResult = rubNum;
                }
            } else {
                convertedResult = "No results, please change the base currency!";
                setNoMatches(true);
            }
            setConverted(convertedResult);
            setOpen(true);
            setPrompt(false);
        }
        return false;
    }

    const defaultSelectValue = () => {
        if (defCurrency?.includes(USD)) {
            return USD;
        }
        return RUB;
    }

    return (
        <Background>
            <Title>Конвертер валют</Title>
            <Wrapper>
                <Select
                    value={defaultSelectValue()}
                    onChange={(e) => handleChange(e)}>
                    <Option>RUB</Option>
                    <Option>USD</Option>
                </Select>
                <Form onSubmit={(e) => conversionCurrency(e, convertArray)}>
                    <Label id="currency">{example}</Label>
                    <Input
                        id="currency"
                        onChange={(e) => setValue(e.target.value)}
                        placeholder="Input format: 10 usd in rub or 10 rub in usd"
                    />

                </Form>
                <ConvertedValue
                    converted={open}
                    style={{ border: `1px solid ${noMatches ? "#ff0000" : "#000000"}` }}>
                    {converted}
                </ConvertedValue>
                <Notification
                    style={{ transform: `${prompt ? "translateY(-100%)" : "translateY(100%)"}` }}>
                    press the <Strong>Enter</Strong> button
                </Notification>
            </Wrapper>
        </Background>
    )
}

export default Converter;