import styled from 'styled-components';
import { useTypedSelector } from '../../../../hooks/useTypedSelector/useTypedSelector';
import { CurrencyItemValues, defaultCurrencyItem, RUB, USD } from '../../../../types/utils';
import { rounding } from '../../../../useFul';

const Item = styled.div`
    display: grid;
    grid-template-columns: 1fr 1fr 1fr;
    align-items: baseline;
    border: 1px solid #000;
    border-radius: 3px;
`;

const Name = styled.h3`
    padding-left: 10px;
`;

const Value = styled.strong``;

interface ExchangeItemProps {
    itemsProps?: CurrencyItemValues
}

function ExchangeItem({ itemsProps }: ExchangeItemProps) {
    const { currencysData } = useTypedSelector(state => state);
    const defCurrency = currencysData.defaultCurrency;

    const renderCurrency = (currency: CurrencyItemValues) => {
        const calcValue = itemsProps && currency.CharCode === RUB ? (1 / itemsProps.Value) : currency.Value;
        return (
            <>
                <Name>{currency.Name}</Name>
                <Value>{currency.CharCode}</Value>
                <Value>
                    1 {currency.CharCode + " "}
                    = {rounding(calcValue, 100, 100)}
                    {" " + defCurrency}
                </Value>
            </>
        )
    }

    return (
        <Item>
            {defCurrency?.includes(RUB) && itemsProps && renderCurrency(itemsProps)}
            {defCurrency?.includes(USD) && renderCurrency(defaultCurrencyItem)}
        </Item>
    )
}

export default ExchangeItem;