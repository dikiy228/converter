import styled from 'styled-components';
import { useTypedSelector } from '../../hooks/useTypedSelector/useTypedSelector';
import { USD } from '../../types/utils';
import ExchangeItem from './components/ExchangeItem/ExchangeItem';

const Background = styled.div`
    width: auto;
    padding: 0 20px;
`;

const Title = styled.h2``;

function Exchange() {
    const { currencysData } = useTypedSelector(state => state);
    const currencys = currencysData.currencys;

    if (currencysData.loading) {
        return <div>Loading...</div>
    }

    return (
        <Background>
            <Title>Курсы валют</Title>
            {currencys && currencys.map(item =>
                item.CharCode === USD &&
                (<ExchangeItem key={Math.random() + 10} itemsProps={item} />)
            )}
        </Background>
    )
}

export default Exchange;
