import { combineReducers } from "redux";
import { currencysReducer } from "./currency";

export const rootReducer = combineReducers({
    currencysData: currencysReducer
});

export type RootState = ReturnType<typeof rootReducer>