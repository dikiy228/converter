import { AppState } from "../../../types/utils";
import { CurrencysAction, CurrencysActionTypes } from "../../types";

const initialState: AppState = {
    loading: false,
    currencys: null,
    error: '',
    defaultCurrency: null,
};

export const currencysReducer = (state = initialState, action: CurrencysAction): AppState => {
    switch (action.type) {
        case CurrencysActionTypes.FETCH_CURRENCYS:
            return { ...state, loading: true, error: null, currencys: null }
        case CurrencysActionTypes.FETCH_CURRENCYS_SUCCESS:
            return { ...state, loading: false, currencys: action.payload }
        case CurrencysActionTypes.FETCH_CURRENCYS_ERROR:
            return { ...state, error: action.payload}
        case CurrencysActionTypes.DEFAULT_CURRENCY:
            return { ...state, defaultCurrency: action.payload}
        default:
            return state;
    }
}