import { Dispatch } from 'react';
import { CurrencysAction, CurrencysActionTypes } from '../../types';

export const defaultCurrency = (currency: string) => {
    return (dispatch: Dispatch<CurrencysAction>) => {
        dispatch({
            type: CurrencysActionTypes.DEFAULT_CURRENCY,
            payload: currency,
        })
    };
};