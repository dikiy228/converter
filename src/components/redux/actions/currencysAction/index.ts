import axios from 'axios';
import { Dispatch } from 'react';
import { Currency, CurrencyItemValues } from '../../../types/utils';
import { CurrencysAction, CurrencysActionTypes } from '../../types';

export const fetchCurrencys = () => {
    return async (dispatch: Dispatch<CurrencysAction>) => {
        try {
            dispatch({
                type: CurrencysActionTypes.FETCH_CURRENCYS,
                loading: true
            })
            const response = await axios.get<Currency>(`https://www.cbr-xml-daily.ru/daily_json.js`);
            const valute = response.data.Valute;
            const currencys: CurrencyItemValues[] = [];
            for (const keys of Object.keys(valute)) {
                const item = valute[keys];
                currencys.push(item);
            }
            dispatch({
                type: CurrencysActionTypes.FETCH_CURRENCYS_SUCCESS,
                loading: false,
                payload: currencys
        })
        } catch (e) {
            dispatch({
                type: CurrencysActionTypes.FETCH_CURRENCYS_ERROR,
                payload: e ? `${e}` : "Произошла ошибка..."
            })
        }
    };
};