import { CurrencyItemValues } from "../../types/utils";

export enum CurrencysActionTypes {
    FETCH_CURRENCYS = 'FETCH_CURRENCYS',
    FETCH_CURRENCYS_SUCCESS = 'FETCH_CURRENCYS_SUCCESS',
    FETCH_CURRENCYS_ERROR = 'FETCH_CURRENCYS_ERROR',
    DEFAULT_CURRENCY = 'DEFAULT_CURRENCY',
}

interface FetchCurrencysAction {
    type: CurrencysActionTypes.FETCH_CURRENCYS;
    loading: true;
}

interface FetchCurrencysSuccessAction {
    type: CurrencysActionTypes.FETCH_CURRENCYS_SUCCESS;
    loading: false;
    payload: CurrencyItemValues[];
}

interface FetchCurrencysErrorAction {
    type: CurrencysActionTypes.FETCH_CURRENCYS_ERROR;
    payload: string;
}

interface DefaultCurrencyAction {
    type: CurrencysActionTypes.DEFAULT_CURRENCY;
    payload: string;
}

export type CurrencysAction = 
    | FetchCurrencysAction
    | FetchCurrencysSuccessAction
    | FetchCurrencysErrorAction
    | DefaultCurrencyAction
