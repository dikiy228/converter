export interface CurrencyItemValues {
    ID: string;
    NumCode: string;
    CharCode: string;
    Nominal: number;
    Name: string;
    Value: number;
    Previous: number;
}

export interface CurrencyItem {
    [key: string]: CurrencyItemValues;
}

export type Currency = {
    Date: string;
    PreviousDate: string;
    PreviousURL: string;
    Timestamp: string;  
    Valute: CurrencyItem;
}

export type AppState = {
    loading: boolean;
    currencys: CurrencyItemValues[] | null;
    error: null | string;
    defaultCurrency: string | null;
}

export const RUB = "RUB";

export const USD = "USD";

export const defaultCurrencyItem: CurrencyItemValues = {
    ID: '',
    NumCode: '',
    CharCode: RUB,
    Nominal: 0,
    Name: "Российский рубль",
    Value: 1,
    Previous: 0,
}