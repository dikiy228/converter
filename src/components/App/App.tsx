import { useEffect } from 'react';
import styled from 'styled-components';
import { Link, Switch, Route, Redirect, useLocation } from 'react-router-dom';
import Converter from '../Pages/Converter/Converter';
import Exchange from '../Pages/Exchange/Exchange';

import './App.css';
import { useDispatch } from 'react-redux';
import { fetchCurrencys } from '../redux/actions/currencysAction';
import { getLanguage } from '../useFul';
import { defaultCurrency } from '../redux/actions/defaultCurrencyAction';
import { RUB, USD } from '../types/utils';

const Container = styled.div`
  width: auto;
  max-width: 1560px;
  margin: 0 auto;
`;

const Header = styled.header`
  width: 100%;
  position: fixed;
  top: 0;
  left: 0;
  background-color: #707070;
`;

const Nav = styled.nav`
  width: 100%;
  padding: 20px 0;
  text-align: center;
  
  a {
    opacity: 0.8;
    cursor: pointer;
    margin-right: 10px;
    font-size: 18px;
    color: #ffffff;
    transition: font-size 0.3s linear;

    &:last-child {
      margin-right: 0;
    }

    &:hover {
      color: #00ff08;
      text-decoration: underline;
      opacity: 1;
      font-size: 130%;
    }
}

`;

const Main = styled.main`
  margin-top: 100px;
`;

const Wrapper = styled.div`
  width: 100%;
  max-width: 1200px;
  margin: 0 auto;
`;

function App() {
  const location = useLocation();
  const dispatch = useDispatch();

  const lang = getLanguage();

  useEffect(() => {
      if (lang.includes("ru")) {
        dispatch(defaultCurrency(RUB));
      }
      if (lang.includes("en")) {
        dispatch(defaultCurrency(USD));
      }
  }, [lang, dispatch])

  useEffect(() => {
    dispatch(fetchCurrencys());
  }, [location.pathname, dispatch])

  return (
    <Container>
      <Header>
        <Nav>
          <Link className={location.pathname === "/converter" ? "active" : ""} to="/converter">Конвертер валют</Link>
          <Link className={location.pathname === "/exchange" ? "active" : ""} to="/exchange">Курсы валют</Link>
        </Nav>
      </Header>
      <Main>
        <Switch>
          <Wrapper>
            <Route path="/converter" component={Converter} />
            <Route path="/exchange" component={Exchange} />
            <Redirect to="converter" />
          </Wrapper>
        </Switch>
      </Main>
    </Container>
  );
}

export default App;
