export const getLanguage = (): string => {
    return (
        (navigator.languages || [])[0]
        //@ts-ignore
        || navigator.userLanguage
        || navigator.language
        //@ts-ignore
        || navigator.browserLanguage
        || 'en'
    )
};

export const rounding = (value: number, num1: number, num2: number) => {
    return Math.floor(value * num1) / num2
}